#!/bin/sh
#
# Display message if command invoked by dmenu exits with a non-zero exit code
#
LOG_FILE=dmenu-msg.log

show_result() {
    echo >> $LOG_FILE
    echo "Exit code: $EXIT_CODE" >> $LOG_FILE
    xmessage -center -buttons ":0" -default "" -file $LOG_FILE
}

dmenu_path | dmenu "$@" | ${SHELL:-"/bin/sh"} > $LOG_FILE 2>&1
EXIT_CODE=$?
[ $EXIT_CODE -eq 0 ] || show_result

rm $LOG_FILE


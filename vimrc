" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages.
runtime! archlinux.vim

" OS-dependant
if has("unix")
    let s:uname = system("uname")
    if s:uname == "FreeBSD\n"
        language messages en_US.UTF-8
    else
        language messages en_US.utf8
    endif
endif

" Appearance
colorscheme slate
set background=dark
syntax on

" Statusbar
set showmode
set rulerformat=%V%=\ %l,%c
set ruler
set wildmenu
set wildmode=list:longest,full

" Navigation keys
nnoremap <Up> gk
nnoremap <Down> gj
nnoremap <ESC>Oa k
nnoremap <ESC>Ob j
nnoremap <ESC>Oc w
nnoremap <ESC>Od b
nnoremap k gk
nnoremap j gj
nnoremap <C-k> k
nnoremap <C-j> j
nnoremap <C-l> w
nnoremap <C-h> b
inoremap <Up> <C-o>gk
inoremap <Down> <C-o>gj
inoremap <ESC>Oa <C-o>k
inoremap <ESC>Ob <C-o>j
inoremap <ESC>Oc <C-o>w
inoremap <ESC>Od <C-o>b

" Custom keys
inoremap jk <Esc>
inoremap <F3> <C-\><C-o>:update<CR>
nnoremap s :update<CR>
nnoremap <C-c> :set hlsearch!<CR>
nnoremap <C-a> :set number!<CR>

" Tabs
set shiftround
set nojoinspaces
set smarttab
set autoindent
set smartindent
command TT4 set tabstop=4 shiftwidth=4 noexpandtab
command TS4 set tabstop=4 shiftwidth=4 expandtab
command TT8 set tabstop=8 shiftwidth=8 noexpandtab
command TS8 set tabstop=8 shiftwidth=8 expandtab
TS4

" Highlight char on col 80 (clear with :2mat)
2mat ErrorMsg '\%80v.'

" Remove trailing white space for all files on save
autocmd BufWritePre * :call <SID>StripTrailingWhitespaces()

" Highlight redundant spaces
highlight RedundantSpaces ctermbg=red guibg=red
match RedundantSpaces /\s\+$\| \+\ze\t/
autocmd InsertEnter * highlight RedundantSpaces ctermbg=NONE guibg=NONE
autocmd InsertLeave * highlight RedundantSpaces ctermbg=red guibg=red

" Line numbers + highlight current
set number
set cursorline
highlight LineNr ctermfg=darkgrey
highlight clear CursorLine

" Mouse
set mouse=a

" Scroll before cursor reaches top/bottom
set scrolloff=5

set nocompatible
set lbr
set nobk
set whichwrap=
set writebackup

" Clipboard
set cb=unnamed
set clipboard=unnamedplus
nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>

" Ctags
set tags=tags;/
map <Tab> 
map <S-Tab> :po<CR>

command Nfo set guifont=Terminal

function! <SID>StripTrailingWhitespaces()
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    call cursor(l, c)
endfun


#!/bin/sh
set -e

THIS_DIR=$(dirname "$0")
CONF_FILE=${THIS_DIR}/i3status-$(hostname -s).conf

# Format to proper JSON i3status format
add_custom(){
	# Escape backslashes and double quotes
	text=$(sed 's#\\#\\\\#g' <<< $1)
	text=$(sed 's#"#\\"#g' <<< $text)

	formatted="{ \"full_text\": \"$text\" },"
	custom+="$formatted"
}

add_spotify_track(){
	spotify_vars=$(sp eval) || return 0
	eval $spotify_vars
	add_custom "$SPOTIFY_ARTIST - $SPOTIFY_TITLE"
}

add_network_usage(){
	add_custom "$(${THIS_DIR}/measure-net-speed.sh 2>/dev/null)"
}

try_dir(){
	pushd $1 >/dev/null 2>&1
}

add_hottest_core(){
	try_dir /sys/devices/platform/coretemp.0/hwmon || return 0
	try_dir hwmon?

	high_temp=0
	high_core=0
	for i in 1 2 3 4 5; do
		[ -f temp${i}_input ] || continue

		temp=$(< temp${i}_input)
		if [ $temp -gt $high_temp ]; then
			high_temp=$temp
			high_core=$((i-1))
		fi
	done

	add_custom "C${high_core} $((high_temp / 1000)) °C"
	popd >/dev/null 2>&1
}

i3status -c $CONF_FILE | (
	read line && echo $line && read line && echo $line &&
	while :
do
	# Full string must begin with [
	custom="["

	add_spotify_track
	add_network_usage
	add_hottest_core

	read line
	echo "${line/[/$custom}"
done)

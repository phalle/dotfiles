#!/bin/sh
set -e
THIS_DIR=$(dirname "$0")
WORK_DIR=$(pwd)

if [ "$WORK_DIR" != "$HOME" ]; then
    echo Please run script from home dir
    exit 1
fi

for src in "$THIS_DIR"/*; do
    [ "$src" = "$0" ] && continue

    src=${src#./}
    dst=.$(basename "$src")

    if [ -L "$dst" -a "$(readlink "$dst")" != "$src" ]; then
        echo -n "Replacing "
        rm "$dst"
    elif [ -e "$dst" ]; then
        echo Skipping $dst
        continue
    fi

    ln -sv "$src" "$dst"
done

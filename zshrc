#
# ~/.zshrc
#

# Load custom environment settings
source ~/.envrc

# Prompt - Add @ in front when logged in remotely
MACHINE='%F{green}%m%f'
if [ -n "$SSH_TTY" ]; then
    MACHINE="%F{red}@%f $MACHINE"
fi

CUR_DIR='%B%F{blue}%~%f%b'
NZ_EXIT=$'%F{red}%(?..[%?]\n)%f'
PROMPT="${NZ_EXIT}${MACHINE}:${CUR_DIR}%# "

# Terminal title (Konsole likes 30, others 0)
set_title() { print -Pn "\e]0;$@\a" }
case $TERM in
    *xterm*|rxvt|(dt|k|E)term)
        precmd()  { set_title "%~" }
        chpwd()   { set_title "%~" }
        preexec() { set_title "$1" }
    ;;
esac

cmd_exists() {
    command -v $1 >/dev/null
    return $?
}

# Misc options
setopt long_list_jobs
unsetopt auto_param_slash

# Command aliases
if [ "$(uname)" = "FreeBSD" ]; then
    alias ls='ls -Gh'
else
    alias ls='ls -h --color=auto'
    alias dmesg='dmesg -L'
    alias locate='locate -Ai'
    alias lftp='eval $(dircolors) ; lftp'
fi

if [ "$HOST" = "pioneer" ]; then
    alias mplayer='mplayer -vo xv'
fi

# Find file name (contains) in current directory
fh() { find . -iname "*$@*" }

# Override shutdown commands to avoid forceful termination of apps
shutdown() { (shutdown-wrapper shutdown $@ &) }
poweroff() { (shutdown-wrapper poweroff $@ &) }
reboot()   { (shutdown-wrapper reboot $@ &) }

# Aliases
alias ll='ls -l'
alias la='ls -Al'
alias lb='ls -ld .*'
alias lsd='ls -d */'
alias lsl='ls -1 | wc -l'
alias tree='tree -C'

if cmd_exists vim; then
    alias vi='vim'
    alias vr='vim -M'
fi

alias diff='colordiff'
alias grep='grep -i --color=auto'
alias egrep='egrep -i --color=auto'

alias df='df -hT'
alias du='du -h --exclude="./.*"'
alias du1='du -d 1'
alias du2='du -d 2'
alias du3='du -d 3'
alias free='free -h'
alias journalctl='journalctl -b -x --no-pager --since=-3600'
alias sc='systemctl --lines=25'
alias jc='journalctl'
alias top='top -o "+%CPU"'
alias mpall='mpstat -P ALL'
alias ff=$'feh --fullscreen --edit --info "echo -e \'%n\n%wx%h\n%S\n%z\n\n%u of %l\'"'

if cmd_exists ionice; then
    alias cp='ionice -c 3 cp'
    alias mv='ionice -c 3 mv'
fi

alias o='xdg-open'
alias gitk='gitk -n 5000'
alias xclip='xclip -sel clip'

alias pacman='pacman --color=auto'
alias pacman-remove-dead-deps='pacman -Qqdt | xargs pacman -R --noconfirm'
alias telnet='telnet -e ^P'
alias ssh='ssh -e ^P'
alias sqlite='sqlite3 -column -header -nullvalue NULL'
alias pkgfile='pkgfile --cachedir=$HOME/.pkgfile'
alias lpa4='lp -o media=a4 -o cpi=12 -o lpi=7.2 -o page-left=48 -o page-right=48 -o page-top=48 -o page-bottom=48'

# Directory navigation
setopt auto_pushd pushd_to_home pushdsilent

alias d='dirs -v'
alias po='popd'

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'

# History
HISTFILE="$HOME/.zsh_history"
HISTSIZE=10000   # Session
SAVEHIST=10000   # Total

setopt inc_append_history hist_ignore_dups hist_find_no_dups

autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

alias h='history'
alias hg="history | grep"
alias hgg="cat $HISTFILE | grep"

cd(){ builtin cd "$@" && pwd > ~/.last_dir }

# Auto completion - might take some time on slower machines
[ -f ~/.zcompdump ] || echo "Creating completion dump file..."
autoload -U compinit
compinit

# Case insensitive completion
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}'

# Dir colors in completion
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

# Colored cat/less/man pages
HL_CMD=src-hilite-lesspipe.sh
if cmd_exists $HL_CMD; then
    alias cat=$HL_CMD
    export LESSOPEN="| $HL_CMD %s"
fi

export LESS="-FRX"

less_termcap() {
  env \
    LESS_TERMCAP_mb=$'\e[1;31m' \
    LESS_TERMCAP_md=$'\e[01;38;5;74m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[1;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[1;32m' \
	$@
}

man() {
	less_termcap man "$@"
}

# Keyboard
# Emacs mode
bindkey -e

# Create a zkbd compatible hash;
# To add other keys to this hash, see: man 5 terminfo
typeset -A key

key[Home]='^[[7~'
key[End]='^[[8~'
key[Insert]=${terminfo[kich1]}
key[Delete]=${terminfo[kdch1]}
key[Up]=${terminfo[kcuu1]}
key[Down]=${terminfo[kcud1]}
key[Left]=${terminfo[kcub1]}
key[Right]=${terminfo[kcuf1]}
key[PageUp]=${terminfo[kpp]}
key[PageDown]=${terminfo[knp]}
key[CtrlLeft]='^[Od'
key[CtrlRight]='^[Oc'

# Set navigation keys
[[ -n "${key[Home]}"      ]]  && bindkey  "${key[Home]}"      beginning-of-line
[[ -n "${key[End]}"       ]]  && bindkey  "${key[End]}"       end-of-line
[[ -n "${key[Insert]}"    ]]  && bindkey  "${key[Insert]}"    overwrite-mode
[[ -n "${key[Delete]}"    ]]  && bindkey  "${key[Delete]}"    delete-char
[[ -n "${key[Up]}"        ]]  && bindkey  "${key[Up]}"        up-line-or-beginning-search
[[ -n "${key[Down]}"      ]]  && bindkey  "${key[Down]}"      down-line-or-beginning-search
[[ -n "${key[Left]}"      ]]  && bindkey  "${key[Left]}"      backward-char
[[ -n "${key[Right]}"     ]]  && bindkey  "${key[Right]}"     forward-char
[[ -n "${key[CtrlLeft]}"  ]]  && bindkey  "${key[CtrlLeft]}"  backward-word
[[ -n "${key[CtrlRight]}" ]]  && bindkey  "${key[CtrlRight]}" forward-word
[[ -n "${key[PageUp]}"    ]]  && bindkey  "${key[PageUp]}"    beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"  ]]  && bindkey  "${key[PageDown]}"  end-of-buffer-or-history

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} )) && (( ${+terminfo[rmkx]} )); then
    function zle-line-init () {
        printf '%s' "${terminfo[smkx]}"
    }
    function zle-line-finish () {
        printf '%s' "${terminfo[rmkx]}"
    }
    zle -N zle-line-init
    zle -N zle-line-finish
fi

[ -f ~/.last_dir ] && [ -d "$(< ~/.last_dir)" ] && cd "$(< ~/.last_dir)"
[ -f ~/.zshrc.local ] && source ~/.zshrc.local

# Prevent shell from reporting non-zero exit code if any of the above tests failed
true
